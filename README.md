## Build and launch

Use 'npm install' to build the project and 'npm start' to launch the app

## What is it?

A personal project to learn and practive Hiragana and Katakana.

Inspired by the duolinguo app.

## Why is it so ugly ?

It's a "quick win" project. Feel free to contribute to make it look better.
