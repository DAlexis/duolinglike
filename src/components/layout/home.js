import React from 'react'
import HalfScreen from './HalfScreen'
import './layout.css'
import Selector from '../Selector'
import myData from '../../data/data.ø.json';
class Home extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {type: "hiragana",data:myData, boolTable: new Array(myData.all.length).fill(false), seedLeft: this.createSeed(myData)};
  }

  createSeed(data) {
    let seed = new Array(data.all.length)
    for (let i = 0; i< data.all.length; i++) {
      seed[i] = (Math.floor(Math.random() * data.all.length))
    }
    return seed;
  }

  handleChange(id, side) {
    let value = false;
    if (side === 'left') {
      value = this.state.right === id ? true : false
    } else if (side === 'right') {
      value = this.state.left ===id ? true : false
    }
    this.setState((state) => { return {[side]:id, boolTable: {...state.boolTable, [id] : value}} })
    
  }
  handleSelectChange(data) {
    this.setState({data: data, boolTable: new Array(data.all.length).fill(false), seedLeft: this.createSeed(data)})
  }
  handleTypeChange(type) {
    this.setState({type: type})
  }
    render() {
    return (

          <div>
            <Selector 
              callbackSelect={data => this.handleSelectChange(data)}
              callbackType={type => this.handleTypeChange(type)}
            />
            <div id="wrapper">
                <HalfScreen 
                type={this.state.type}
                values={this.state.data}
                seed={this.state.seedLeft}
                hide={this.state.boolTable} 
                onChange={(value) => this.handleChange(value,'left')} /> 

                <HalfScreen 
                type="roman" 
                values={this.state.data}
                hide={this.state.boolTable} 
                onChange={(value) => this.handleChange(value,'right')} />
              </div>
          </div>
            );
    }
  }

  export default Home;