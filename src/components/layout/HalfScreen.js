import React from 'react'
import {ButtonToolbar, ToggleButtonGroup, ToggleButton } from 'react-bootstrap'

class HalfScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = this.props.seed[i];
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array
}

  getAll(){
    let elements =  this.props.values.all.map( character => {
      return (
        <ToggleButton className="char-button"
          disabled={(this.props.type !== 'roman') && this.props.hide[character.id]} 
          value={character.id}
          key={character.id}
          variant="primary"
          size="lg"
          >  {character[this.props.type]} 
    
        </ToggleButton>
      )
    })
    return elements
  }
    render() {
      let elements = this.getAll();
      if (this.props.seed) {
        elements = this.shuffleArray(elements)
      }
      
      return <div className="halfScreen">
              <ButtonToolbar>
                <ToggleButtonGroup className="char-group" type="radio" name="options" onChange={this.props.onChange}>
                  {elements}
                </ToggleButtonGroup>
              </ButtonToolbar>
          </div>
    }
  }

  export default HalfScreen;