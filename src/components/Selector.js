import React from 'react'

import all from '../data/data.json';
import ø from '../data/data.ø.json';
import k from '../data/data.k.json';
import s from '../data/data.s.json';
import t from '../data/data.t.json';
import n from '../data/data.n.json';
import h from '../data/data.h.json';
import m from '../data/data.m.json';
import y from '../data/data.y.json';
import r from '../data/data.r.json';
import w from '../data/data.w.json';



class Selector extends React.Component {
    constructor(props) {
      super(props);
      this.state = {data: {}}
    }


    handleClick(e) {
        this.props.callbackSelect(this.state.data[e.target.value])
    }
    handleClickType(e) {
        this.props.callbackType(e.target.value)
    }

    componentDidMount() {
        this.setState({data: {ø:ø,all:all,k:k,s:s,t:t,n:n,h:h,m:m,y:y,r:r,w:w}})
    }

    render() {
        return (
            <div>
                <select name="selector" onChange={(e) => this.handleClick(e)}>
                    <option value="ø">ø</option>
                    <option value="all">All</option>
                    <option value="k">k</option>
                    <option value="s">s</option>
                    <option value="t">t</option>
                    <option value="n">n</option>
                    <option value="h">h</option>
                    <option value="m">m</option>
                    <option value="y">y</option>
                    <option value="r">r</option>
                    <option value="w">w</option>
                </select>

                <select name="type" onChange={(e) => this.handleClickType(e)}>
                    <option value="hiragana">hiragana</option>
                    <option value="katakana">katakana</option>
                </select>

            </div>
        )
    }

}


export default Selector;